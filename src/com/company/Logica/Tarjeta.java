package com.company.Logica;

import java.util.Date;

public class Tarjeta extends MedioDePago{
    private int numeroDeTarjeta;
    private String nombreDelCuentaHabiente;
    private Date fechaDeExpiracion;
    private int codigoPostal;
    private int cvc;

    public Tarjeta() {
    }

    public int getNumeroDeTarjeta() {
        return numeroDeTarjeta;
    }

    public void setNumeroDeTarjeta(int numeroDeTarjeta) {
        this.numeroDeTarjeta = numeroDeTarjeta;
    }

    public String getNombreDelCuentaHabiente() {
        return nombreDelCuentaHabiente;
    }

    public void setNombreDelCuentaHabiente(String nombreDelCuentaHabiente) {
        this.nombreDelCuentaHabiente = nombreDelCuentaHabiente;
    }

    public Date getFechaDeExpiracion() {
        return fechaDeExpiracion;
    }

    public void setFechaDeExpiracion(Date fechaDeExpiracion) {
        this.fechaDeExpiracion = fechaDeExpiracion;
    }

    public int getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(int codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public int getCvc() {
        return cvc;
    }

    public void setCvc(int cvc) {
        this.cvc = cvc;
    }

    @Override
    public String toString(){
        return  null;
    }
}
