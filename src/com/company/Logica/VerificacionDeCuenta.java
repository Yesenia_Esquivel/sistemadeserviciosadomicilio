package com.company.Logica;

public class VerificacionDeCuenta {
    private int id;
    private int codigoDeVerificacion;
    private Cuenta cuentaVerificada;
    public VerificacionDeCuenta() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCodigoDeVerificacion() {
        return codigoDeVerificacion;
    }

    public void setCodigoDeVerificacion(int codigoDeVerificacion) {
        this.codigoDeVerificacion = codigoDeVerificacion;
    }

    public Cuenta getCuentaVerificada() {
        return cuentaVerificada;
    }

    public void setCuentaVerificada(Cuenta cuentaVerificada) {
        this.cuentaVerificada = cuentaVerificada;
    }
    @Override
    public String toString(){
        return  null;
    }
}
