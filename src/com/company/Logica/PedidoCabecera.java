package com.company.Logica;

import java.util.Date;

public class PedidoCabecera {
    private int id;
    private Date fechaDeCreacion;
    private String estado;
    private Cuenta cuentaDelCliente;
    private Cuenta cuentaDelConductor;
    private DetalleDePedido[]detalleDePedido;
    private double montoTotal;

    public PedidoCabecera() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFechaDeCreacion() {
        return fechaDeCreacion;
    }

    public void setFechaDeCreacion(Date fechaDeCreacion) {
        this.fechaDeCreacion = fechaDeCreacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Cuenta getCuentaDelCliente() {
        return cuentaDelCliente;
    }

    public void setCuentaDelCliente(Cuenta cuentaDelCliente) {
        this.cuentaDelCliente = cuentaDelCliente;
    }

    public Cuenta getCuentaDelConductor() {
        return cuentaDelConductor;
    }

    public void setCuentaDelConductor(Cuenta cuentaDelConductor) {
        this.cuentaDelConductor = cuentaDelConductor;
    }

    public DetalleDePedido[] getDetalleDePedido() {
        return detalleDePedido;
    }

    public void setDetalleDePedido(DetalleDePedido[] detalleDePedido) {
        this.detalleDePedido = detalleDePedido;
    }

    public double getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(double montoTotal) {
        this.montoTotal = montoTotal;
    }
    @Override
    public String toString(){
        return  null;
    }
}
