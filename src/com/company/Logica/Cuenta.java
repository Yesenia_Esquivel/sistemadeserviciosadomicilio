package com.company.Logica;

import java.util.Date;

public class Cuenta {
    private int id;
    private String nombre;
    private String apellidos;
    private String email;
    private String password;
    private Date fechaDeNacimiento;
    private int edad ;
    private String genero;
    private String celular;
    private String urlFotoDePerfil;
    private String estado;
    private MedioDeTransporte[] mediosDeTransporteRegistrados;
    private MedioDePago[] mediosDePagoRegistrados;
    private PedidoCabecera[] historialDePedidos;
    public Cuenta() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getFechaDeNacimiento() {
        return fechaDeNacimiento;
    }

    public void setFechaDeNacimiento(Date fechaDeNacimiento) {
        this.fechaDeNacimiento = fechaDeNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getUrlFotoDePerfil() {
        return urlFotoDePerfil;
    }

    public void setUrlFotoDePerfil(String urlFotoDePerfil) {
        this.urlFotoDePerfil = urlFotoDePerfil;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public MedioDeTransporte[] getMediosDeTransporteRegistrados() {
        return mediosDeTransporteRegistrados;
    }

    public void setMediosDeTransporteRegistrados(MedioDeTransporte[] mediosDeTransporteRegistrados) {
        this.mediosDeTransporteRegistrados = mediosDeTransporteRegistrados;
    }

    public MedioDePago[] getMediosDePagoRegistrados() {
        return mediosDePagoRegistrados;
    }

    public void setMediosDePagoRegistrados(MedioDePago[] mediosDePagoRegistrados) {
        this.mediosDePagoRegistrados = mediosDePagoRegistrados;
    }

    public PedidoCabecera[] getHistorialDePedidos() {
        return historialDePedidos;
    }

    public void setHistorialDePedidos(PedidoCabecera[] historialDePedidos) {
        this.historialDePedidos = historialDePedidos;
    }
    @Override
    public String toString(){
        return  null;
    }
}
