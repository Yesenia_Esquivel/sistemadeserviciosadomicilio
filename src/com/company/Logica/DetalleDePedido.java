package com.company.Logica;

public class DetalleDePedido {
    private int id;
    private int numDeServiciosEfectuados;
    private Servicio servicioEfectuado;
    private double importe;


    public DetalleDePedido() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumDeServiciosEfectuados() {
        return numDeServiciosEfectuados;
    }

    public void setNumDeServiciosEfectuados(int numDeServiciosEfectuados) {
        this.numDeServiciosEfectuados = numDeServiciosEfectuados;
    }

    public Servicio getServicioEfectuado() {
        return servicioEfectuado;
    }

    public void setServicioEfectuado(Servicio servicioEfectuado) {
        this.servicioEfectuado = servicioEfectuado;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }
    @Override
    public String toString(){
        return  null;
    }
}
