package com.company.Logica;

public class Direccion {
    private int id;
    private String direccionEspecifica;
    private Provincia provincia;

    public Direccion() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDireccionEspecifica() {
        return direccionEspecifica;
    }

    public void setDireccionEspecifica(String direccionEspecifica) {
        this.direccionEspecifica = direccionEspecifica;
    }

    public Provincia getProvincia() {
        return provincia;
    }

    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }
    @Override
    public String toString(){
        return  null;
    }


}
