package com.company.Logica;

public class Provincia {
    private int id;
    private String nombre;
    private Canton[]cantones;

    public Provincia() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Canton[] getCantones() {
        return cantones;
    }

    public void setCantones(Canton[] cantones) {
        this.cantones = cantones;
    }
    @Override
    public String toString(){
        return  null;
    }
}
