package com.company.Logica;

public abstract class MedioDeTransporte {
     private int id;
     private String numPlaca;
     private String tipo;

     public MedioDeTransporte() {
     }

     public int getId() {
          return id;
     }

     public void setId(int id) {
          this.id = id;
     }

     public String getNumPlaca() {
          return numPlaca;
     }

     public void setNumPlaca(String numPlaca) {
          this.numPlaca = numPlaca;
     }

     public String getTipo() {
          return tipo;
     }

     public void setTipo(String tipo) {
          this.tipo = tipo;
     }
}
