package com.company.Logica;

public class Canton {
    private int id;
    private String nombre;
    private Distrito[] distritos;

    public Canton() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Distrito[] getDistritos() {
        return distritos;
    }

    public void setDistritos(Distrito[] distritos) {
        this.distritos = distritos;
    }
    @Override
    public String toString(){
        return  null;
    }
}
